﻿using Microsoft.EntityFrameworkCore;

namespace WebAPIRestFull
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) {

        }
    }
}
