﻿using Microsoft.AspNetCore.Mvc;
using WebAPIRestFull.Entidades;

namespace WebAPIRestFull.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UbigeoController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public UbigeoController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Obtener()
        {
            var ubigeo = new Ubigeo();
            return Ok(ubigeo);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> ObtenerPorId(int id)
        {
            var ubigeo = new Ubigeo();
            return Ok(ubigeo);
        }

        [HttpPost]
        public async Task<IActionResult> Registrar([FromBody] Ubigeo ubigeo)
        {
            return Ok(new { statusCode = 1, message = "Se registro con éxito" });
        }

        [HttpPut]
        public async Task<IActionResult> Editar(Ubigeo ubigeo)
        {
            return Ok(new { statusCode = 1, message = "Se registro con éxito" });
        }
    }
}
