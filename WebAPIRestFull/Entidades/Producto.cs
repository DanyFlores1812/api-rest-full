﻿using System.ComponentModel.DataAnnotations;
using WebAPIRestFull.Validaciones;

namespace WebAPIRestFull.Entidades
{
    public class Ubigeo : IValidatableObject
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo nombre es requerido.")]
        //[FirstCapitalLetter]
        public string Nombre { get; set; }
        public string Abreviatura { get; set; }
        public bool Activo { get; set; }
        public string UsuarioCreador { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int Mayor { get; set; }
        public int Menor { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var value = Nombre;

            if (value != null)
            {
                var primeraletra = value.ToString()[0].ToString();
                if (primeraletra != primeraletra.ToUpper())
                {
                    yield return new ValidationResult("La primera letra debe ser mayuscula", new string[] { nameof(Nombre) });
                }
            }

            if(Menor > Mayor)
            {
                yield return new ValidationResult("El campo menor no puede ser mayor al campo mayor", new string[] { nameof(Menor) });
            }
        }
    }

}
